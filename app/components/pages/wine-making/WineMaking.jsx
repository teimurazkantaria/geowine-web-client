import React from 'react';

export default function (props) {
    return (
        <div className="app-qvevri container-fluid">
            <h1 className="app-page-header">Qvevri wine making technology</h1>
            <div className="row">
                <div className="col-md-6">
                    <img src="images/wine-making1.jpg" className="img-responsive text-center" />
                </div>
                <div className="col-md-6 app-wine-making-varieties">
                    <h3>There are 500 local varieties of vines  in Georgia.</h3>

                    <p>All varieties of wine grapes distribution zone has unique features and characteristics.</p>

                    <p>Grape varieties for Industrial production in Eastern Georgia  are: “Rkhatsiteli”, “Saperavi”, “Khikhvi”, “Kisi”, “Tavkveri”, “Chinese”, “Goreli Mtsvane”, “Shavi Kapistoni “and others.
                    </p><p>East Georgia is famous for it’s  grape varieties such as:” Tsolikauri”, “Aleksndrouli”, “Khrakhuna”,” Ojaleshi”, “Khachachi”, “Tsulikidzis Tetra” and etc.</p>

                    <p>With wine  grape varieties, there are a variety of hybrid ones, grown in Georgia  which are not used for wine-making, because wine made of this grape, contains toxic substances.</p>

                    <p>Historically, several methods  of qvevri  wine makinging existed in Georgia , each can be significant, mainly, according to characteristic  features of a  zone. For example,  In Kakheti region  alcoholic fermentation involves  total number of (“Chacha” in Georgian),Imeretian wine fermentation includes adding  5-8 per cent of grappa ,on sweet wine which   is poured in qvevri s.</p>
                </div>
            </div>
            <hr />
            <div className="row">
                <div className="col-md-6">
                    <h3>Kakhetinian wine making method</h3>
                    <p>Wine making in qvevri , requires great deal of labour, faith in the job, love  and responsibility toward ancestors or future generation. That’s why local wine color emits heat of  the sun's rays .</p>

                    <p>The local method of qvevri  wine fermentation   involves grapes processing  simulteniously with  grappa and stems. After alcoholic fermentation, in 5-6 month duration, white wine turns into  grappa, when the red  one needs  1-2 months  .</p>

                    <p>Grapes,  for wine-making, are  picked when being  well riped. At the same time, soft maturity is necessary which is expressed in amber color.</p>

                    <p>According to the  traditional method, grapes, are placed in winepress. </p>
                </div>
                <div className="col-md-6">
                    <img style={{marginTop: '20px'}} src="images/wine-making2.jpg" className="img-responsive" />
                </div>
            </div>
            <div className="row" style={{marginTop: '20px'}}>
                <div className="col-md-6">
                    <p>During stirring process mass aeration and formations of  body shaping  takes place.</p>

                    <p>After alcohol fermentation, grappa emits carbon dioxide on the bottom of the vessel , while wine substance remains above.   If necessary, vessel   is filled with the same type of wine and  qvevri  is covered non hermetically. 3-4 days later, the qvevri  throat  is processed with sulfur smoke  and closed hermetically. Grappa is removed when  wine gets taste,  color and  from   stems.</p>

                    <p>First of all,”  fresh faction, formed above grappa , is removed to another qvevri    where fermentation starts. Wine is gaining unique features, distinctive aroma and flavor. </p>

                    <p>While  fermentation wine  is under constant organoleptic and laboratory control. Attention is paid to the process of development and the quality of the wine.</p>

                    <p>Mature wines are bottled and sealed with  cork stopper.  In the glass bottle, wine continues to evolve.  As more time passes, the taste of the wine improves, the bouquet becomes better, smoother and more pleasant to drink .</p>
                </div>
                <div className="col-md-6">
                    <p>IT’s a large-sized vessel for pressing grapes. It used to  be made of lime, chestnut, elm, walnut wood. In old times   it  used to be curved in  stone  or rock . </p>

                    <p>he ancient cave presses, date back to the Hellenistic period (second millennium BC, Zeniti ).  They can  be found in the early and middle feudal  ages  as well.</p>

                    <p>While pressing , sweet grapes flow into the qvevri . Having been fermented in air , stems are added.</p>

                    <p>Stems contain 10 percent of the high-molecular substances - lignin. In the air oxidation, it forms aromatic aldehydes in  order to improve future wine and flavors blended with wine.</p>

                    <p>Special attention is given to crush fermentation process. Crush  has  to be stirred 5-6 times a day, with the special tool.  It excludes rising grappa above that can  produce unwanted microbiological environment.</p>
                </div>
            </div>

            <hr />

            <div className="row">
                <div className="col-md-6">
                    <h3>Imeretian wine making method</h3>
                    <p>Imeretian method  of qvevri  wine  making, involves  sweet fermentation  in  qvevri , with the addition of 5-8% of the grappa. After 5-6 months  wine is poured into another qvevri  for aging.  </p>

                    <p>AS many century-old experience and practice has shown, adding definite amount of grappa  while fermentation, positively affects on  Imeretian wine  stability and organoleptic values. Tanninum consistence of grappa, binds protein substances and other colloids. Tannin allocated  in sediment, takes away the unwanted microns. Such wines are well cleaned and naturally stable.. </p>

                    <p>While vintage, grapes with 20-23% of the level of sugar are selected and  picked . Grapes  are pressed and sweet substance flows into  qvevri , where 5-8% alcohol is added. Excessive alcohol hardens Imeretian wine.</p>

                    <p>After filling the qvevri ,  space for fermentation  is left, volume of  which consists of 10-15% .  50 cm long”  aerial “is  made and qvevri  is  closed  with chestnut and oak valve. </p>

                    <p>While fermentation, grappa stirring process takes place  and 3-4 time aeration, strengthens time of yeast vitality.</p>

                    <p>After strong fermentation, qvevri  is filled with the same content  of young  wine and is  blocked by a  valve.  At this stage the valve is non hermtically  closed , which promotes gas disservice.</p>

                    <p>Wine, made according to Imeretian technology,  is famous for a variety of aromas, flavors and fresh sour. The local wine, seems to be alike Imeretian man by it’s nature and matches with  their cuisine. </p>
                </div>
                <div className="col-md-6">
                    <img style={{marginTop: '20px'}} src="images/wine-making3.jpg" className="img-responsive" />
                </div>
            </div>

        </div>
    );
}

