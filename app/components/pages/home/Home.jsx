import React from 'react';

export default function (props) {
    return (
        <div className="app-home">
            <div className="app-home-image">
                <img src="images/background-home.jpg" className="img-responsive" alt="Responsive image" />
            </div>
            <div className="app-home-content">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Georgia is one of the oldest wine regions in the world</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <p>
                            Wine lovers have a lot to thank Georgia for. It is widely believed that this is where wine production first began, over 8000 years ago.
                            The vine is central to Georgian culture and tightly bound to their religious heritage. It is common for families throughout Georgia to grow their own grapes and produce wine. Feasting and hospitality are central pillars of Georgian culture, and traditional banquets are presided over by a toastmaster, or Tamada, who proposes numerous toasts throughout the meal, and ensures the wine flows liberally.
                        </p>

                        <p>Georgia is a land famed for its natural bounty. These days there are over 500 species of grape in Georgia, a greater diversity than anywhere else in the world, with around 40 of these grape varieties being used in commercial wine production. Conditions are well suited for viticulture: summers are rarely excessively hot, winters are mild and frost-free. In addition, the mountains around the vineyards are full of natural springs, and rivers drain mineral-rich waters into the valleys. All this means that Georgian wines have a reputation for being exceptionally pure.</p>
                        <p>
                            Around 150 million litres of wine are produced each year in Georgia, with around 45 000 hectares of vineyards under cultivation. There are 18 Specific Viticulture Areas (a local analogy of the Controlled Appellations of Origin) where the grape variety, planting density and yield per hectare is controlled by Ministry of Agriculture, and where the grape yield per hectare is limited to 8 tons.
                        </p>
                    </div>
                    <div className="col-md-6">
                        <p>
                            Georgia’s wines fall into several zones: Kakheti and Kartli in the east, and Imereti, Samegrelo, Guria, Ajaria, and Abkhazia in the west. By far the most important of these is Kakheti, which produces 70% of all Georgian wine. The map below shows where these can be found, along with the most important Specific Viticulture Areas.
                        </p>
                        <p>
                            Around 150 million litres of wine are produced each year in Georgia, with around 45 000 hectares of vineyards under cultivation. There are 18 Specific Viticulture Areas (a local analogy of the Controlled Appellations of Origin) where the grape variety, planting density and yield per hectare is controlled by Ministry of Agriculture, and where the grape yield per hectare is limited to 8 tons.
                        </p>
                        <p>
                            Georgia’s wines fall into several zones: Kakheti and Kartli in the east, and Imereti, Samegrelo, Guria, Ajaria, and Abkhazia in the west. By far the most important of these is Kakheti, which produces 70% of all Georgian wine. The map below shows where these can be found, along with the most important Specific Viticulture Areas.
                        </p>
                    </div>
                </div>
            </div>
            <div className="container" style={{marginTop: '30px'}}>
                <p className="text-center">Tbilisi, the capital of Georgia</p>
                <img src="images/background-home-2.png" className="img-responsive" alt="Responsive image" />
            </div>
        </div>
    );
}

