import React from 'react';
import { connect } from 'react-redux';
import * as shoppingApi from '../../../api/shoppingApi';
import ShoppingCart from './ShoppingCart'

class ShoppingCartContainer extends React.Component {

    componentDidMount() {

        console.log('DID MOUNT');
        shoppingApi.getShoppingCartItems(this.props.userDetails.customer.id);
    }

    render() {
        return (
            <ShoppingCart items={this.props.items} totalSum={this.props.totalSum} />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        items: store.shoppingCartState.items,
        userDetails: store.userState.userDetails,
        totalSum: store.shoppingCartState.totalSum
    }
}

export default connect(mapStateToProps)(ShoppingCartContainer)