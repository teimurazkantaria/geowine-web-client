import React from 'react';

export default function (props) {
    return (
        <div className="app-shopping-cart">
            <h1>Shopping Cart</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Sum</th>
                    </tr>
                </thead>
                <tbody>
                    {props.items.map(item => {
                        return (
                            <tr key={item.itemInfo.id}>
                                <td>{item.itemInfo.product_id}</td>
                                <td>{item.product.title}</td>
                                <td>$ {item.itemInfo.price}</td>
                                <td>{item.itemInfo.quantity}</td>
                                <td>{item.itemInfo.sum}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <strong>Total: $ {props.totalSum} </strong>
            </div>
            <div style={{marginTop: '20px'}}>
                <a className="btn btn-primary">
                    Checkout
                </a>
            </div>
        </div>
    );
}

