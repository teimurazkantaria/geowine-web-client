import React from 'react';

export default function (props) {
    return (
        <div className="app-qvevri container-fluid">
            <div className="row">
                <div className="col-md-6">
                    <div className="app-qvevri-image">
                        <img src="images/qvevri.jpg" className="img-responsive" />
                    </div>
                </div>
                <div className="col-md-6 app-article">
                    <p>
                        Wine making culture in Georgia counts 8000 continuous history and itís proved
                        by various archeological discoveries and historic facts. A lot of exhibits
                        related to wine making, dated thousands years of exisatnce are preserved in
                        meuseums of Georgia.
                    </p>
                    <p>
                        In the south region of Georgia archaeologists found grape seeds characteristic
                        for BC. Sec. 6th millennium and according to several morphological and ampelographic
                        features, grape vine species ñ were awarded as "Vitis Vinifera Sativa"
                    </p>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 app-article">
                    <p>
                        The wild grape -Vitis Vinifera Silrestris is still widespread in Georgia. In the
                        early 80-ies, the species of wild vine, entered the ìRed Bookî of Georgia as the
                        object of state protection. Together with wild vines, more than 500 Georgian vine
                        varieties are depicted in Georgia, 430 ones of which are protected by the state and
                        private collectorsí vineyards.
                    </p>
                    <p>
                        Grape leftovers, discovered by the archeologists, date back to the mentioned
                        period and they are the oldest around the globe, that proves once again that
                        Georgia is a homeland of vine. Scientists believe that the word Ghvino (wine, vin, vine)
                        has Georgian origin. 500 out of world-known 2.000 grape species, are Georgian
                    </p>
                </div>
                <div className="col-md-6">
                    <img src="images/qvevri2.jpg" className="img-responsive" />
                </div>
            </div>

            <h3>Qvevri</h3>
            <div className="row">
                <div className="col-md-6">
                    <p>
                        Qvevri is a unique large vessel made of clay which is kept deeply under ground for wine storage. The history of it’s invention counts back 8 thousand years.

                        Qvevri is unique for it’s ability of merging with local natural features. Chemical and clay minerals composition, location of springs, climate , skillful and sophisticated methods of it’s making and scorching, provided Georgian wine making.

                        2013, UNESCO awarded the intangible cultural heritage status to Georgian wine qvevri distillation.
                    </p>
                </div>
                <div className="col-md-6">
                    <p>
                        Quevris are not the same as amphora – firstly, they are much larger, and are buried in the ground up their neck, to preserve a stable temperature. Secondly, the entire wine making process takes place within the quevri – initial fermentation right through to maturation, with the fermenting grape juice often being left on the skins and even grape stems to produce wines of exceptional flavour and complexity. Quevris are used differently in different areas of Georgia, often depending on the climate of the region. As the regions get hotter the more skins and stems tend to be fermented with the grapes – if stems were left in the wine in the cooler regions it would produce wines that were far too “green” and harsh.
                    </p>
                </div>
            </div>
        </div>
    );
}

