import React from 'react';
import * as constants from '../../../constants/constants';
import NumericInput from 'react-numeric-input';

export default class ProductTile extends React.Component {

    constructor(props) {
        super(props);
        this.quantity = 1;
    }

    quantityChanged(value) {
        this.quantity = value;
    }

    addProductToShoppingCart(event) {
        this.props.addProductToShoppingCart(this.props.product.id, this.quantity);
    }

    render()
    {
        return (
            <div className="product-tile">
                <div className="row">
                    <div className="col-md-3">
                        <div className="product-image">
                            <img src={constants.PRODUCT_IMAGES_BASE_URL + '/' + this.props.product.image}/>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="product-title">
                            {this.props.product.title}
                        </div>
                        <hr />
                        <div className="product-short-description">
                            {this.props.product.short_description}
                        </div>
                    </div>
                    <div className="col-md-2 product-tile-price-details">
                        <div>
                            Price: ${this.props.product.price}
                            <div className="product-actions">
                                <form className="form">
                                    <div className="form-group">
                                        <NumericInput
                                            className="form-control"
                                            min={1}
                                            max={100}
                                            value={1}
                                            onChange={this.quantityChanged.bind(this)} />
                                    </div>
                                    <div className="form-group">
                                        <a onClick={this.addProductToShoppingCart.bind(this)} className="btn btn-default">
                                            Add to Cart
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
