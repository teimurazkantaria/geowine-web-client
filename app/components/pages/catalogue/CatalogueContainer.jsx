import React from 'react';
import Catalogue from './Catalogue'
import { connect } from 'react-redux';
import * as catalogueApi from '../../../api/catalogueApi';
import * as shoppingApi from '../../../api/shoppingApi';

class CatalogueContainer extends React.Component {

    componentDidMount() {
        catalogueApi.getProducts();
    }

    addProductToShoppingCart(productId, quantity) {
        shoppingApi.addProductToShoppingCart(this.props.userDetails.customer.id, productId, quantity);
    }

    render() {
        return (
            <Catalogue products={this.props.products} addProductToShoppingCart={this.addProductToShoppingCart.bind(this)} />
        );
    }
}

const mapStateToProps = function (store) {
    return {
        products: store.catalogueState.products,
        userDetails: store.userState.userDetails
    }
}

export default connect(mapStateToProps)(CatalogueContainer)