import React from 'react';
import ProductTile from './ProductTile'

export default function (props) {
    return (
        <div className="app-catalogue">
            <div className="row">
                {props.products.map(product => {
                    return (
                        <div className="col-md-12" key={product.id}>
                            <ProductTile
                                product={product}
                                addProductToShoppingCart={props.addProductToShoppingCart} />
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

