import React from 'react';
import { connect } from 'react-redux';
import Categories from './Categories';

/**
 * @see https://css-tricks.com/learning-react-redux/
 * "... Container Component Omission.
 * Sometimes, a Container Component only needs to subscribe to the store and it doesn't need any methods like
 * componentDidMount() to kick off Ajax requests. It may only need a render() method to pass state down to the
 * Presentational Component. In this case, we can make a Container Component this way: ..."
 */
const mapStateToProps = function (store) {
    return {
        categories: store.categoriesState.categories
    }
}

export default connect(mapStateToProps)(Categories)