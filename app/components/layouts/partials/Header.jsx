import React from 'react';
import { Link } from 'react-router';

export default class Header extends React.Component {

    render() {
        return (
            <nav className="app-header navbar navbar-default navbar-fixed-top">
                <div className="container-fluid">

                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className="navbar-brand" to="/">
                            {/*<div className="app-logo"></div>*/}
                            GeoWine
                        </Link>
                    </div>

                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul className="nav navbar-nav">
                            <li><Link to="/catalogue" activeClassName="active">CATALOGUE</Link></li>
                        </ul>

                        <ul className="nav navbar-nav">
                            <li><Link to="/history" activeClassName="active">HISTORY</Link></li>
                            <li><Link to="/wine-making" activeClassName="active">WINE MAKING</Link></li>
                        </ul>

                        <ul className="nav navbar-nav navbar-left header-shopping-cart">
                            <li>
                                <Link to="/shopping-cart" activeClassName="active">
                                    <span className="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                                    &nbsp; SHOPPING CART $ {this.props.shoppingCart.totalSum}
                                </Link>
                            </li>
                        </ul>

                        <ul className="nav navbar-nav navbar-right header-shopping-cart">
                            <li>
                                <a> Welcome, {this.props.userDetails.customer.first_name}</a>
                            </li>
                            <li>
                                <a>Balance: $ {this.props.userDetails.customer.balance}</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>
        )
    }
}