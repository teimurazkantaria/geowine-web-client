import React from 'react';

export default function (props) {
    return (
        <div className="app-categories">
            <ul className="nav">
                {props.categories.map(category => {
                    return (
                        <li key={category.id} role="presentation"><a>{category.title}</a></li>
                    );
                })}
            </ul>
        </div>
    );
}
