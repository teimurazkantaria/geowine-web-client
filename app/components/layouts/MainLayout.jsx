import React from 'react';
import HeaderContainer from './partials/HeaderContainer';

export default function (props) {
    return (
        <div className="container-fluid app-main-layout">
            <HeaderContainer />
            <div style={{marginTop: '-7px'}}>
                <div className="app-content">
                    {props.children}
                </div>
            </div>
        </div>
    );
}
