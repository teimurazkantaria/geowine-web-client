import React from 'react';
import HeaderContainer from './partials/HeaderContainer';
import CategoriesContainer from './partials/CategoriesContainer';

export default function (props) {
    return (
        <div className="container-fluid app-layout-with-categories">
            <HeaderContainer/>
            <div className="app-categories-placeholder">
                <CategoriesContainer/>
            </div>
            <div style={{marginTop: '-7px'}}>
                <div className="app-content">
                    {props.children}
                </div>
            </div>
        </div>
    );
}