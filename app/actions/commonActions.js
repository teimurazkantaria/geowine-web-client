import * as types from '../actions/actionTypes'

export function applicationInitSuccess(data) {
    return {
        type: types.APPLICATION_INIT_SUCCESS,
        data
    }
}
