import * as types from '../actions/actionTypes'

export function addProductToShoppingCartSuccess(shoppingCartData) {
    return {
        type: types.ADD_PRODUCT_TO_SHOPPING_CART_SUCCESS,
        shoppingCartData
    }
}

export function getShoppingCartItemsSuccess(items) {
    return {
        type: types.GET_SHOPPING_CART_ITEMS_SUCCESS,
        items
    }
}
