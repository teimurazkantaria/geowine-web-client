import * as types from '../actions/actionTypes'

export function getCatalogueProductsSuccess(products) {
    return {
        type: types.GET_CATALOGUE_PRODUCTS_SUCCESS,
        products
    }
}
