import axios from 'axios';
import store from '../store';
import * as shoppingCartActions from '../actions/shoppingActions';
import * as constants from '../constants/constants';

/**
 * Add product to the shopping cart
 */
export function addProductToShoppingCart(customerId, productId, quantity) {
    return axios.post(constants.API_BASE_URL + '/shopping/add-product-to-cart', {
        customerId: customerId,
        productId: productId,
        quantity: quantity
    })
    .then(response => {
        console.log('-----------------------');
        console.log(response);
        response.data.shoppingCart.totalSum = response.data.shoppingCart.total_sum;
        store.dispatch(shoppingCartActions.addProductToShoppingCartSuccess(response.data));
        return response;
    });
}

/**
 * Get shopping cart items with products
 * @param customerId
 */
export function getShoppingCartItems(customerId) {
    return axios.get(constants.API_BASE_URL + '/shopping/shopping-cart/' + customerId + '/items')
        .then(response => {
            console.log(response);
            const items = response.data;
            store.dispatch(shoppingCartActions.getShoppingCartItemsSuccess(items));
            return response;
        });
}

