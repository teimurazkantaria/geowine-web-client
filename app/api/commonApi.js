import axios from 'axios';
import store from '../store';
import * as commonActions from '../actions/commonActions';
import * as constants from '../constants/constants';

/**
 * Initialize application
 */
export function init() {
    return axios.get(constants.API_BASE_URL + '/common/init')
        .then(response => {
            console.log('------ INIT ---------');
            console.log(response);
            store.dispatch(commonActions.applicationInitSuccess(response.data));
            return response;
        });
}
