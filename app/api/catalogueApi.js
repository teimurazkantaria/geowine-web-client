import axios from 'axios';
import store from '../store';
import * as catalogueActions from '../actions/catalogueActions';
import * as constants from '../constants/constants';

/**
 * Get products
 * @return {axios.Promise}
 */
export function getProducts() {
    return axios.get(constants.API_BASE_URL + '/catalogue/products')
        .then(response => {
            store.dispatch(catalogueActions.getCatalogueProductsSuccess(response.data));
            return response;
        });

}