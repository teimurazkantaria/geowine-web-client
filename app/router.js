import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import AppContainer from './components/AppContainer';
import MainLayout from './components/layouts/MainLayout';
import LayoutWithCategories from './components/layouts/LayoutWithCategories';
import HomeContainer from './components/pages/home/HomeContainer';
import HistoryContainer from './components/pages/history/HistoryContainer';
import WineMakingContainer from './components/pages/wine-making/WineMakingContainer'; import CatalogueContainer from './components/pages/catalogue/CatalogueContainer';
import ShoppingCartContainer from './components/pages/shopping-cart/ShoppingCartContainer'
import store from './store';
import * as commonApi from './api/commonApi';
/**
 * Initialize application data from the server
 * @param dispatch
 * @return {function(*, *, *)}
 */
function initialize(dispatch) {
    return (nextState, replace, callback) => {
        commonApi.init()
            .then(() => {
                callback();
            });
    };
}

export default (
    <Router history={browserHistory}>
        <Route component={AppContainer} onEnter={initialize(store.dispatch)}>
            <Route component={MainLayout}>
                <Route path="/" component={HomeContainer} />
                <Route path="/history" component={HistoryContainer} />
                <Route path="/wine-making" component={WineMakingContainer} />
            </Route>
            <Route component={LayoutWithCategories}>
                <Route path="/catalogue" component={CatalogueContainer} />
                <Route path="/shopping-cart" component={ShoppingCartContainer} />
            </Route>
        </Route>
    </Router>
);