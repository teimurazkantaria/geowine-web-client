import * as types from '../actions/actionTypes';

const initialState = {
    userDetails: {
        user: {},
        customer: {}
    }
};

const userReducer = function(state = initialState, action) {

    switch(action.type) {

        case types.APPLICATION_INIT_SUCCESS:
            return { ...state, userDetails: action.data.userDetails};
    }

    return state;
}

export default userReducer;
