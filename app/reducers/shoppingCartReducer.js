import * as types from '../actions/actionTypes';

const initialState = {
    items: [],
    totalSum: 0
};

const shoppingCartReducer = function(state = initialState, action) {

    switch(action.type) {

        case types.ADD_PRODUCT_TO_SHOPPING_CART_SUCCESS:
            return { ...state, totalSum: action.shoppingCartData.shoppingCart.totalSum};

        case types.APPLICATION_INIT_SUCCESS:
            // total_sum - response from api
            // TODO:: convert to camel case on response or in api before sent
            const totalSum = action.data.shoppingCart ? action.data.shoppingCart.total_sum : 0;
            return { ...state, totalSum: totalSum}

        case types.GET_SHOPPING_CART_ITEMS_SUCCESS:
            return { ...state, items: action.items}
    }

    return state;
}

export default shoppingCartReducer;
