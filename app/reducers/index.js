import { combineReducers } from 'redux';

// Reducers
import categoriesReducer from './categoriesReducer';
import catalogueReducer from './catalogueReducer'
import shoppingCartReducer from './shoppingCartReducer'
import userReducer from './userReducer'

// Combine Reducers
var reducers = combineReducers({
    categoriesState: categoriesReducer,
    catalogueState: catalogueReducer,
    shoppingCartState: shoppingCartReducer,
    userState: userReducer
});

export default reducers;