import * as types from '../actions/actionTypes';

const initialState = {
    categories: []
};

const categoriesReducer = function(state = initialState, action) {

    switch(action.type) {

        case types.APPLICATION_INIT_SUCCESS:
            return { ...state, categories: action.data.categories};
    }

    return state;
}

export default categoriesReducer;
