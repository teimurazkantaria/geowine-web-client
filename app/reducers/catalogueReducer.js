import * as types from '../actions/actionTypes';

const initialState = {
    products: []
};

const catalogueReducer = function(state = initialState, action) {

    switch(action.type) {

        case types.GET_CATALOGUE_PRODUCTS_SUCCESS:
            return { ...state, products: action.products};
    }

    return state;
}

export default catalogueReducer;
